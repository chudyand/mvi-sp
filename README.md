# Doporučovanie obrázkov k textu

## Zadnie 
Cieľom tejto práce bolo zostrojiť webovú aplikáciu, ktorá bude schopná doporučovať obrázky na základe textu, ktorý môže predstavovať napríklad článok z Wikipedie. Prvá časť úlohy teda predstavovala vykonať tagovanie obrázkov podľa obsahu daného obrázka a druhá časť úlohy bola definovať objekty pojednávané v texte a prepojenie týchto objektov so známymi tagmi v databáze obrázkov.

## Potrebné data
#### Word2vec model
```sh
$ cd image_recommendations
$ mkdir model
$ cd model 
$ wget https://s3.amazonaws.com/dl4j-distribution/GoogleNews-vectors-negative300.bin.gz
$ gunzip GoogleNews-vectors-negative300.bin
```
#### Image dataset 
```sh
$ cd image_recommendations
$ mkdir image_dataset
$ cd image_dataset
$ wget https://www.dropbox.com/s/lzadbqrmo1dqrcc/image_dataset.zip?dl=0
$ tar -xvf image_dataset.zip 
```

#### Install 
Install and run flask server 
```sh
$ cd image_recommendations
$ chmod +x install_venv.sh
$ ./install_venv.sh    # A script which creates a python3 virtualenv and installs there all of the requirements needed for running this project
$ export FLASK_APP=app.py
$ flask run
```


