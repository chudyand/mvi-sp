
$(document).ready(function(){

    $('#sub-button').click(function(e) {
        let $btn = $(this);
        $('#result').empty();

        $btn.button('loading');
        let text = $("#comment").val().toString();
        console.log(text);
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = response;
        xhttp.open("POST", "http://127.0.0.1:5000/text", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(text);
    });
});


function response() {


    if (this.readyState === 4 && this.status === 200) {
        let images = JSON.parse(this.responseText);
        $('#sub-button').button('reset');
        for (let image of images) {
            console.log(image);
            $('#result').append(
                "<div  class=\"col-lg-3 col-md-4 col-xs-6\">" +
                "<a href=\"http://127.0.0.1:5000/image/" +image + "\" class=\"d-block mb-4 h-100\">" +
                "<img class=\"img-fluid img-thumbnail\" src=\"http://127.0.0.1:5000/image/" +image + "\"  alt=\"\">" +
                "</a>" +
                "</div>");
        }
    }



}

