let input = $("#input-id");


input.fileinput({
    uploadAsync: true,
    uploadUrl: "/",
    showAjaxErrorDetails: false,
    allowedFileExtensions: ["jpg", "png"],
    showPreview: false,
    showCaption: true

});
