import os
import json

from werkzeug.utils import secure_filename
from flask import Flask, render_template, request, send_file
from apscheduler.schedulers.background import BackgroundScheduler

# Own files
from src.recommender import Recommender
from src.image_clasification import run_inference_on_image

app = Flask(__name__)
recommender_model = Recommender()
app.config['UPLOAD_FOLDER'] = './tagging queue/'
app.config['DATASET'] = './image_dataset/'


@app.route('/', methods=['GET'])
def home_page():
    """ API for homepage
    """
    return render_template("index.html", images=[])


@app.route('/', methods=['POST'])
def upload_file():
    """ API for uploading new image files
    """
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])
    try:
        file = request.files['file']
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    except IOError:
        return json.dumps({"status": "ERROR"}), 400, {'ContentType': 'application/json'}

    return json.dumps({"status": "OK"}), 200, {'ContentType': 'application/json'}


@app.route('/upload', methods=['GET'])
def upload_page():
    """ API for upload section page
    """
    return render_template("upload.html", images=[])


@app.route('/text', methods=['POST'])
def text_input():
    """ API for send text to processing
    """
    text = request.get_data().decode('UTF-8')
    images = recommender_model.recommend_image(text)
    return json.dumps(images), 200, {'ContentType': 'application/json'}


@app.route('/image/<string:name>', methods=['GET'])
def get_image(name=None):
    """ API for load image from dataset on page
    """
    return send_file("./image_dataset/" + name, mimetype='image/gif')


def crone():
    """ Crone function for check files for tagging and inclusion into dataset
    """
    if not os.path.exists(app.config['UPLOAD_FOLDER']):
        os.makedirs(app.config['UPLOAD_FOLDER'])

    images = [f for f in os.listdir(app.config['UPLOAD_FOLDER'])
              if os.path.isfile(os.path.join(app.config['UPLOAD_FOLDER'], f))]

    for img_name in images:
        try:
            tags = run_inference_on_image(app.config['UPLOAD_FOLDER'] + "/" + img_name)
        except:
            continue

        print("New image tags: " + tags)
        recommender_model.add_image(img_name, tags, serialize=True)
        os.rename(app.config['UPLOAD_FOLDER'] + img_name, app.config['DATASET'] + img_name)


cr = BackgroundScheduler(daemon=True)
cr.add_job(crone, 'interval', minutes=1)
cr.start()  # Start of crone

if __name__ == '__main__':
    app.run()
